package com.missfarukh.touchlistener.ui.home;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.widget.Toast;

import com.missfarukh.touchlistener.R;
import com.missfarukh.touchlistener.service.TouchService;

public class HomeFragment extends Fragment {

    public final static int REQUEST_OVERLAY_PERMISSION = 10101;
    public final static int REQUEST_SYSTEM_WRITE = 11000;
    private HomeViewModel homeViewModel;
    Button btnStart, btnStop;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        final TextView textView = root.findViewById(R.id.text_home);
        btnStart = root.findViewById(R.id.btnStart);
        btnStop = root.findViewById(R.id.btnStop);

        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });


        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeViewModel.getText().setValue("Start Service ? ");
                btnStart.setVisibility(View.GONE);
                btnStop.setVisibility(View.VISIBLE);

                Intent intent = new Intent(getActivity(), TouchService.class);
                getActivity().startService(intent);
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeViewModel.getText().setValue("STOP Service ? ");
                btnStop.setVisibility(View.GONE);
                btnStart.setVisibility(View.VISIBLE);

                Intent intent = new Intent(getActivity(), TouchService.class);
                getActivity().stopService(intent);

            }
        });

       checkDrawOverlayPermission();

       //TEST THIS
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            boolean settingsCanWrite = Settings.System.canWrite(getActivity());
            if(!settingsCanWrite) {
                // If do not have write settings permission then open the Can modify system settings panel.
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                startActivityForResult(intent, REQUEST_SYSTEM_WRITE);
            }
        }

        return root;
    }

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(getActivity())) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getActivity().getPackageName()));
            startActivityForResult(intent, REQUEST_OVERLAY_PERMISSION);
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_OVERLAY_PERMISSION) {
            //check whether permission was granted
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                if (!Settings.canDrawOverlays(getActivity())) {

                    btnStart.setVisibility(View.GONE);
                    btnStop.setVisibility(View.GONE);

                } else {

                    Log.d("Debug", "Overlay permission valid");
                    btnStart.setVisibility(View.VISIBLE);
                    btnStop.setVisibility(View.GONE);

                }
            }
        }

        if(requestCode == REQUEST_SYSTEM_WRITE) {
            if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M && Settings.System.canWrite(getActivity())) {
                Toast.makeText(getActivity(), "System Write Settings granted", Toast.LENGTH_SHORT).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        if(isMyServiceRunning(TouchService.class)) {
            btnStart.setVisibility(View.GONE);
            btnStop.setVisibility(View.VISIBLE);
        } else {
            btnStart.setVisibility(View.VISIBLE);
            btnStop.setVisibility(View.GONE);
        }
        super.onResume();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}