package com.missfarukh.touchlistener.ui.dashboard;

import android.content.Context;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.annotation.Nullable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.missfarukh.touchlistener.R;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    Button btnClick;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        btnClick = root.findViewById(R.id.btnClick);

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);
                    r.play();

                    //TEST THIS
                    Configuration config = new Configuration();
                    config.fontScale = 2.0f;
                    getResources().getConfiguration().setTo(config);

                    Settings.System.putFloat(getActivity().getBaseContext().getContentResolver(), Settings.System.FONT_SCALE, (float) 2.0);

                    AudioManager audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
                    int vol = audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
                    vol++;
                    audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, vol, 0);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return root;
    }
}